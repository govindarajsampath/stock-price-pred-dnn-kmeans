import pandas as pd
from sklearn.model_selection import train_test_split
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.momentum import RSIIndicator
from ta.trend import MACD
from numpy import loadtxt
# from keras.models import Sequential
# from keras.layers import Dense
import tensorflow as tf
from sklearn.cluster import KMeans
import numpy as np
from sklearn.preprocessing import scale
# Load datas
from tensorflow.python.feature_column.feature_column_v2 import categorical_column_with_hash_bucket
from tensorflow.python.tpu.feature_column import embedding_column
import pickle as p

map = {0: [], 1: [], 2: [], 3: []}

names = ['rsi_price_8', 'rsi_price_13', 'rsi_volume_8', 'rsi_volume_13', 'macd_fast_price', 'macd_slow_price']

df = pd.read_csv('train.csv', sep=',')
df.drop(columns=['DATE', 'index', 'OPEN', 'HIGH', 'LOW', 'CLOSE', 'VOLUME'], inplace=True)

# df.dropna(subset = ['OPEN','HIGH','LOW','CLOSE','VOLUME','rsi_price_8','rsi_price_13','rsi_volume_8','rsi_volume_13','macd_fast_price','macd_slow_price'], inplace=True)
# X = pd.DataFrame(scale(df))

X = df.drop(columns=['next'])
X.dropna(subset=['rsi_price_8', 'rsi_price_13', 'rsi_volume_8', 'rsi_volume_13', 'macd_fast_price', 'macd_slow_price'],
         inplace=True)
kmeans = KMeans(n_clusters=4, random_state=True)
kmeans.fit(X)

fX = df.drop(columns=['next'])
fX.dropna(subset=['rsi_price_8', 'rsi_price_13', 'rsi_volume_8', 'rsi_volume_13', 'macd_fast_price', 'macd_slow_price'],
          inplace=True)
cX = kmeans.predict(fX)
df.dropna(subset=['rsi_price_8', 'rsi_price_13', 'rsi_volume_8', 'rsi_volume_13', 'macd_fast_price', 'macd_slow_price',
                  'next'], inplace=True)
for i in range(len(df)):
    map[cX[i]].append((df.iloc[i]))
f = open("pickled.txt", "wb")
p.dump(kmeans, f)
f.close()

#
# estimator = tf.estimator.DNNRegressor(
#     feature_columns=[categorical_feature_a_emb, categorical_feature_b_emb,categorical_feature_d_emb, categorical_feature_c_emb,categorical_feature_e_emb, categorical_feature_f_emb],
#     hidden_units=[1024, 512, 256])

# Or estimator using the ProximalAdagradOptimizer optimizer with
# regularization.
estimator = tf.estimator.DNNRegressor(
    feature_columns=[map[0]],
    hidden_units=[1024, 512, 256],
    optimizer=tf.compat.v1.train.ProximalAdagradOptimizer(
        learning_rate=0.1,
        l1_regularization_strength=0.001
    ))

# # Or estimator using an optimizer with a learning rate decay.
# estimator = tf.estimator.DNNRegressor(
#     feature_columns=[categorical_feature_a_emb, categorical_feature_b_emb,categorical_feature_d_emb, categorical_feature_c_emb,categorical_feature_e_emb, categorical_feature_f_emb],
#     hidden_units=[1024, 512, 256],
#     optimizer=lambda: tf.keras.optimizers.Adam(learning_rate=tf.compat.v1.train.exponential_decay(learning_rate=0.1,global_step=tf.compat.v1.train.get_global_step(),decay_steps=10000,decay_rate=0.96)))
#
# # Or estimator with warm-starting from a previous checkpoint.
# estimator = tf.estimator.DNNRegressor(
#     feature_columns=[categorical_feature_a_emb, categorical_feature_b_emb,categorical_feature_d_emb, categorical_feature_c_emb,categorical_feature_e_emb, categorical_feature_f_emb],
#     hidden_units=[1024, 512, 256],
#     warm_start_from="/path/to/checkpoint/dir")

# Input builders
[tr, te] = train_test_split(map[0], shuffle=True)
[te, eval] = train_test_split(te, shuffle=True)
print("----------------------")


def input_fn_train(is_shuffle=None):
    x = []
    y = []
    for o in range(len(tr)):
        x.append(tr[:6])
        y.append(tr[6])

    def parser(record):
        keys_to_features = {
            'label': y,
            'features': x
        }
        parsed = tf.io.parse_single_example(record, keys_to_features)
        my_features = {}
        for idx, names in range(4):
            my_features[names] = parsed['features'][idx]
        return my_features, parsed['label']

    dataset= parser(tr)
    dataset = dataset.batch(32)
    iterator = dataset.make_one_shot_iterator()
    features, labels = iterator.get_next()
    return features, labels


def input_fn_eval():
    # Returns tf.data.Dataset of (x, y) tuple where y represents label's class
    # index.
    x = []
    y = []
    for i in range(len(eval)):
        x.append(eval[:6])
        y.append(eval[6])
    def parser(record):
        keys_to_features = {
            'label': y,
            'features': x
        }
        parsed = tf.io.parse_single_example(record, keys_to_features)
        my_features = {}
        for idx, names in range(4):
            my_features[names] = parsed['features'][idx]
        return my_features, parsed['label']

    dataset= parser(eval)
    dataset = dataset.batch(32)
    iterator = dataset.make_one_shot_iterator()
    features, labels = iterator.get_next()
    return features, labels


def input_fn_predict():
    # Returns tf.data.Dataset of (x, None) tuple.
    x = []
    y = []
    for i in range(len(te)):
        x.append(te[:6])

    def parser(record):
        keys_to_features = {
            'label': y,
            'features': x
        }
        parsed = tf.io.parse_single_example(record, keys_to_features)
        my_features = {}
        for idx, names in range(4):
            my_features[names] = parsed['features'][idx]
        return my_features, parsed['label']

    dataset = parser(te)
    dataset = dataset.batch(32)
    iterator = dataset.make_one_shot_iterator()
    features, labels = iterator.get_next()
    return features, None


estimator.train(input_fn=input_fn_train)
metrics = estimator.evaluate(input_fn=input_fn_eval)
predictions = estimator.predict(input_fn=input_fn_predict)
