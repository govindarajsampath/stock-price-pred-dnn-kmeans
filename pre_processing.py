import pandas as pd
from sklearn.model_selection import train_test_split
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.momentum import RSIIndicator
from ta.trend import MACD
from sklearn.cluster import KMeans
import numpy as np

# Load datas
df = pd.read_csv('data.csv', sep=',')

# Clean NaN values
df = dropna(df)

print(df.head())

df["rsi_price_8"] = RSIIndicator(close=df["CLOSE"], n=8).rsi()
df["rsi_price_13"] = RSIIndicator(close=df["CLOSE"], n=13).rsi()
df["rsi_volume_8"] = RSIIndicator(close=df["VOLUME"], n=8).rsi()
df["rsi_volume_13"] = RSIIndicator(close=df["VOLUME"], n=13).rsi()

df["macd_fast_price"] = MACD(close=df["CLOSE"], n_fast=8, n_slow=13).macd()
df["macd_slow_price"] = MACD(close=df["CLOSE"], n_fast=13, n_slow=21).macd()

df.sort_values(by=['DATE'], inplace=True)

next = df["CLOSE"]
next.drop(next.head(5).index,inplace=True) # drop first n rows
df.drop(df.tail(5).index,inplace=True) # drop last n rows

df.reset_index(inplace=True)
# n = next.reset_index()

df["next"] = pd.Series(next.to_numpy())

# print(df.index.values)

print("-----------------")

#IST 9:15 - 9:45 AM
#IST 3:25 - 3:30 PM

df["DATE"] = pd.to_datetime(df["DATE"])
print("----------")
time_mask = (df['DATE'].dt.hour < 15) & (df['DATE'].dt.minute < 15)
# print(df[time_mask])
# print(len(df[time_mask]))
# print(len(time_mask))

df.drop(df[time_mask].index.values, inplace=True)

mask = (df['DATE'].dt.hour > 20) & (df['DATE'].dt.minute > 45)

df.drop(df[mask].index.values, inplace=True)

[train, test] = train_test_split(df, shuffle=True)

pd.DataFrame(train).to_csv("train.csv")
pd.DataFrame(test).to_csv("test.csv")

# df.to_csv("processed.csv")